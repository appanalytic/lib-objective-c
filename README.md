# AppAnalyticsObjC

[![CI Status](http://img.shields.io/travis/appanalytic/lib-objective-c.svg?style=flat)](https://travis-ci.org/appanalytic/lib-objective-c)
[![Version](https://img.shields.io/cocoapods/v/AppAnalyticsObjC.svg?style=flat)](http://cocoapods.org/pods/AppAnalyticsObjC)
[![License](https://img.shields.io/cocoapods/l/AppAnalyticsObjC.svg?style=flat)](http://cocoapods.org/pods/AppAnalyticsObjC)
[![Platform](https://img.shields.io/cocoapods/p/AppAnalyticsObjC.svg?style=flat)](http://cocoapods.org/pods/AppAnalyticsObjC)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
Enable http domains exception
info.plist >> add App Transport Security Settings ++>> Allow Arbitrary Loads = YES

## Installation

AppAnalyticsObjC is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "AppAnalyticsObjC"
```


Add this line of code in 'viewControlle.m' file:
```objectivec
#import "AppAnalyticsObjC.h"
```

## Start
Make an object and call submitCampain method:
```objectivec
AppAnalyticsObjC *object = [[AppAnalyticsObjc alloc] initWithAccessKey: @"Your ACCESS-KEY"];
[object submitCampaign];
```

## Author

AppAnalytics Develpment Team, development@appanalytics.ir

## License

AppAnalyticsObjC is available under the MIT license. See the LICENSE file for more info.
